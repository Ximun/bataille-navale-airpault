#!/usr/bin/python3

import socket
import select
import threading
import pickle

lsock = []
selectPlayer = {}
players = []
observators= []

""" principal server socket """
def setSocketConnect():
	s = socket.socket(socket.AF_INET6,socket.SOCK_STREAM,0)
	s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
	s.setsockopt(socket.SOL_TCP,socket.TCP_NODELAY,1)
	s.bind(('',8765))
	s.listen(1)

	return s

""" return the selected sockets """
def listSocket(s):
	listen,write,error = select.select(lsock+[s],[],[])
	return listen

""" new client accept process """
def socketAccept(socketServer):
		client,addr = socketServer.accept()
		lsock.append(client)
		if len(selectPlayer) < 2:
			selectPlayer[len(selectPlayer)] = client
			if len(selectPlayer) == 1:
			  sendMsg(client, 'ECHO', 'Bienvenue. En attente d\'un autre joueur pour démarrer la partie...')
			players.append(client)
		else:
			observators.append(client)
		return client

""" close client socket process """
def socketDisconnect(sck_client):
	sck_client.close()
	lsock.remove(sck_client)

""" send part of the main communication system between sockets, based on command/arguments """
def sendMsg(socket, command, data = ''):
    msg = pickle.dumps([command, data])
    socket.send(msg)

""" receive part of the main communication system between sockets, based on command/arguments """
def readMsg(msg):
    msg = pickle.loads(msg)
    return (msg[0], msg[1])
