#!/usr/bin/python3

from socket_link import *
from game import *
import random

currentPlayer = 0
playing = False
finished = False

""" generate a random valid configuration """
def randomConfiguration():
    boats = [];
    while not isValidConfiguration(boats):
        boats=[]
        for i in range(NB_BOATS):
            x = random.randint(1,WIDTH)
            y = random.randint(1,WIDTH)
            isHorizontal = random.randint(0,1) == 0
            boats = boats + [Boat(x,y,LENGTHS_REQUIRED[i],isHorizontal)]
    return boats

""" return a game board matrix """
def displayConfiguration(boats, shots=[], showBoats=True):
    Matrix = [[" " for x in range(WIDTH+1)] for y in range(WIDTH+1)]
    for i  in range(1,WIDTH+1):
        Matrix[i][0] = chr(ord("A")+i-1)
        Matrix[0][i] = i

    if showBoats:
        for i in range(NB_BOATS):
            b = boats[i]
            (w,h) = boat2rec(b)
            for dx in range(w):
                for dy in range(h):
                    Matrix[b.x+dx][b.y+dy] = str(i)

    for (x,y,stike) in shots:
        if stike:
            Matrix[x][y] = "X"
        else:
            Matrix[x][y] = "O"

    return Matrix

""" send game boards to a client """
def sendBoards(socket, msg, board1, board2):
    sendMsg(socket, msg, [board1, board2])

""" The normal game process: send each board to players and observators """
def playProcess(player):
    opponent = (player+1)%2
    confPlayer = displayConfiguration(game.boats[player], game.shots[opponent], showBoats=True)
    confOpponent = displayConfiguration([], game.shots[player], showBoats=False)
    sendBoards(selectPlayer.get(player), 'PLAY', confPlayer, confOpponent)
    obsProcess(observators)

""" Send the all game state to a socket list """
def obsProcess(socketList):
    if socketList:
        conf0 = displayConfiguration(game.boats[0], game.shots[1], showBoats=True)
        conf1 = displayConfiguration(game.boats[1], game.shots[0], showBoats=True)
        for obs in socketList:
            sendBoards(obs, 'BOARD', conf0, conf1)
    
""" Play a new random shot """
def randomNewShot(shots):
    (x,y) = (random.randint(1,10), random.randint(1,10))
    while not isANewShot(x,y,shots):
        (x,y) = (random.randint(1,10), random.randint(1,10))
    return (x,y)

""" The end game process """
def endGame():
    global finished
    winner = gameOver(game)
    finished = True
    
    endMsg = 'La partie est terminée. La victoire revient au joueur %i.' % (winner+1)
    print(endMsg)
    
    obsProcess(lsock)
    sendMsg(selectPlayer.get(winner), 'END', 'GAGNÉ! :)')
    sendMsg(selectPlayer.get((winner+1)%2), 'END', 'PERDU! :(')
    for obs in observators:
        sendMsg(obs, 'END', endMsg)

""" The main game process, including the select socket loop """
def main():
    global playing
    global currentPlayer
    global game

    print("Création de la socket de connexion...")
    socketServer = setSocketConnect()
    print("Socket créée, écoute sur le port 8765")

    while not finished:
        listeSocket = listSocket(socketServer)
        for socketListen in listeSocket:

            if socketListen == socketServer:
                client = socketAccept(socketServer)
                if len(players) == 2 and not playing:
                    for i in range(2):
                        player = selectPlayer.get(i)
                        sendMsg(player, 'START', (WIDTH, LENGTHS_REQUIRED, LENGTH_CARDINALITIES_REQUIRED, NB_BOATS, TOTAL_LENGTH, i))
                elif len(players) == 2 and playing:
                    sendMsg(client, 'ECHO', 'Une partie est déjà en cours. Vous devenez observateur...')

            elif socketListen in players and not playing:
                data = socketListen.recv(1500)
                if data == b'':
                    socketDisconnect(socketListen)
                else:
                    command, arg = readMsg(data)
                    if command == 'CONF':
                        if selectPlayer[0] == socketListen:
                            boats0 = arg
                        elif selectPlayer[1] == socketListen:
                            boats1 = arg
                        if 'boats0' in locals() and 'boats1' in locals():
                            game = Game(boats0, boats1)
                            playProcess(currentPlayer)
                            playing = True

            elif socketListen in players and playing:
                data = socketListen.recv(1500)
                player = selectPlayer.get(currentPlayer)
                if data == b'':
                    socketDisconnect(socketListen)
                else:
                    command, arg = readMsg(data)
                    if command == 'SHOOT' and socketListen == player:
                        addShot(game, arg[0], arg[1], currentPlayer)
                        if gameOver(game) != -1:
                            endGame()
                            break
                        currentPlayer = (currentPlayer+1)%2
                        playProcess(currentPlayer)
    
main()
