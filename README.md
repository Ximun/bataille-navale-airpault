# Bataille Navale Airpault

Projet de réseau de la licence Pro ADSILLH 2019/2020.

## Installation

Clonage du dépôt:

```bash
git clone git@framagit.org:Ximun/bataille-navale-airpault.git
cd bataille-navale-airpault
```

## Utilisation

### Exécution des programmes

Le jeu est une bataille navale fonctionnant en réseau. Il utilise un protocole
reposant sur un système de **client / serveur**.

```bash
# Lancer un serveur:
./main.py

# Lancer un client (adresse du serveur en paramètre), doit supporter IPv6
./client.py mon-super-domaine.lol
```

### Configuration

Le serveur tourne avec la configuration par défaut. Il est possible d'adapter le
jeu et choisir sa propre configuration en modifiant le fichier `game.py`

```bash
WIDTH = 10 # Largeur de la grille de jeu
LENGTHS_REQUIRED = [2,3,3,4,5] # Liste des tailles différentes de bateaux
LENGTH_CARDINALITIES_REQUIRED = [0,0,1,2,1,1] # Tableau de vérification des tailles
```

## Dépendances

`client.py` et `main.py` nécessitent tous deux la présence de `game.py` et
`socket_link.py` dans leur dossier pour fonctionner.

Ce jeu fonctionne avec `python 3`.

## Journal

[Wiki du journal de
projet](https://framagit.org/Ximun/bataille-navale-airpault/wikis/journal)
