#!/usr/bin/python3

import socket
import pickle
import sys, os
import random
from socket_link import sendMsg, readMsg
from game import *

if len(sys.argv) < 2:
    print("Veuillez entrer l'adresse du serveur de Bataille Navale...")
    sys.exit(os.EX_USAGE)
else:
    hote = sys.argv[1]

port = 8765
finished = False

socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)

""" generate a random valid configuration """
def randomConfiguration():
    boats = [];
    while not isValidConfiguration(boats):
        boats=[]
        for i in range(NB_BOATS):
            x = random.randint(1,WIDTH)
            y = random.randint(1,WIDTH)
            isHorizontal = random.randint(0,1) == 0
            boats = boats + [Boat(x,y,LENGTHS_REQUIRED[i],isHorizontal)]
    return boats

""" ask player to play a shoot """
def makeShot():
    x_char = input ("quelle colonne? ")
    x_char = x_char.capitalize()
    x = ord(x_char)-ord("A")+1
    y = int(input ("quelle ligne? "))
    print("...En attente...")
    return x,y

""" display a matrix on stdout """
def printMatrix(Matrix):
    for y in range(0, WIDTH+1):
        if y == 0:
            l = "  "
        else:
            l = str(y)
            if y < 10:
                l = l + " "
        for x in range(1, WIDTH+1):
            l = l + str(Matrix[x][y]) + " "
        print(l)

""" a simple echo function """
def echo(arg=""):
    print(arg)

""" starting game process: boats generation """
def start(arg=""):
    global WIDTH
    global LENGTHS_REQUIRED
    global LENGTH_CARDINALITIES_REQUIRED
    global NB_BOATS
    global TOTAL_LENGTH

    WIDTH, LENGTHS_REQUIRED, LENGTH_CARDINALITITES_REQUIRED, NB_BOATS, TOTAL_LENGTH, number = arg
    print("La partie commence! Vous êtes le joueur %i" % (number+1))
    boats = randomConfiguration()
    # send Conf
    sendMsg(socket, 'CONF', boats)

""" ingame main process: display game an make a shoot """
def play(arg=""):
    displayBoard(arg)
    sendMsg(socket, 'SHOOT', makeShot())

""" display an entire game board """
def displayBoard(arg=""):
    printMatrix(arg[0])
    print("---------------------")
    printMatrix(arg[1])
    print("---------------------")

""" end game process """
def end(arg=''):
    global finished
    print(arg)
    finished = True
    
commands = {
  'ECHO': echo,
  'PLAY': play,
  'START': start,
  'BOARD': displayBoard,
  'END': end,
}

""" first server connection """
try:
    socket.connect((hote,port))
except ConnectionRefusedError as e:
    print("Impossible de se connecter au serveur", e)
    exit(1)

""" main game loop """
while not finished:
    try:
        data = socket.recv(1500)
    except Exception as e:
        print("Connexion avec le serveur perdue!")
        sys.exit(1)
    if data == b'':
        socket.close()
    else:
        """ Receive command an arg from server, then handle it """
        command, arg = readMsg(data)
        try:
            commands[command](arg)
        except Exception as e:
            print("Erreur...", e)
            sys.exit(1)

socket.close()
